from logging import getLogger

from vengeance.bot import Bot, BotSetting

LOG = getLogger(__name__)


class FeralCountdownBot(Bot):
	"""
	Periodically announces how much time remains until the next feral horde.
	Notification frequency picks up (by default) the day before the horde, and even more
	so on the day of the horde. Frequency for each phase (day of, day before, more than
	a day) is all configurable.
	"""
	notify_frequency_day_of = BotSetting(
		default=5*60,
		description='How often (in seconds) feral horde arrival is announced the day of'
	)
	notify_frequency_day_before = BotSetting(
		default=10*60,
		description='How often (in seconds) feral horde arrival is announced the day prior'
	)
	notify_frequency_more_than_day = BotSetting(
		default=15*60,
		description='How often (in seconds) feral horde arrival is announced when more than a day remains'
	)
