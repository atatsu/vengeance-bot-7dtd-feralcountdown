import os
import re
from setuptools import setup, find_packages


def read_requirements(req_type):
	requirements = []
	req_file = os.path.abspath(os.path.join(
		os.path.dirname(__file__), 'requirements', '{}.txt'.format(req_type)
	))
	with open(req_file, 'r') as req_file:
		reqs = req_file.read().split()
		for req in reqs:
			if '-r' in req or '-e' in req:
				continue
			elif not req:
				continue
			requirements.append(req)
	return requirements


def read_version():
	re_version = re.compile(r"^__version__\W*=\W*'([\d.abrc]+)'")
	init_py = os.path.join(
		os.path.dirname(__file__), 'vengeance_bot_7dtd_feralcountdown', '__init__.py'
	)
	with open(init_py) as init_file:
		for line in init_file:
			match = re_version.match(line)
			if match is not None:
				return match.group(1)

		raise RuntimeError('Cannot find version in vengeance_bot_7dtd_feralcountdown/__init__.py')


setup(
	author='atatsu',
	author_email='nathan.lundquist@gmail.com',
	classifiers=[
		'Development Status :: 3 - Alpha',
		'Environment :: Console',
		'License :: OSI Approved :: MIT License',
		'Operating System :: OS Independent',
		'Programming Language :: Python',
		'Programming Language :: Python :: 3.6',
	],
	description='Vengeance bot for the 7 Days to Die protocol.',
	entry_points={
		'vengeance.bot.7dtdprotocol': [
			'feralcountdownbot = vengeance_bot_7dtd_feralcountdown:FeralCountdownBot'
		]
	},
	install_requires=read_requirements('common'),
	keywords=['games', 'bots', '7 Days to Die'],
	license='MIT',
	name='vengeance-protocol-7dtd',
	packages=['vengeance_bot_7dtd_feralcountdown'],
	python_requires='>=3.6',
	tests_require=read_requirements('common') + read_requirements('test'),
	version=read_version(),
)
